#!/bin/sh

virtualenv .venv
pip install -r requirements.txt
alembic upgrade heads
python3 __main__.py