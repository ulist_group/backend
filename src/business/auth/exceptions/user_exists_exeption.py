from src.infrastucture.exceptions.business_exception import BusinessException


class UserExistsException(BusinessException):
    def __init__(self):
        super().__init__(message='User exists', errors=[], code=1000)