from src.infrastucture.exceptions.business_exception import BusinessException


class InvalidCredentialsException(BusinessException):
    def __init__(self):
        super().__init__(message='Invalid credentials', errors=[], code=1010)