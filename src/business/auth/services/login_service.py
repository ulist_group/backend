from passlib.context import CryptContext
from datetime import datetime, timedelta
from src.infrastucture.db.db_connection import DbConnection
from src.infrastucture.jwt import jwt as Jwt
import jwt as JWT
from src.infrastucture.config.config import Config

from src.business.auth.exceptions.invalid_credentials_exception import InvalidCredentialsException

pwd_context = CryptContext(
    schemes=["pbkdf2_sha256"],
    default="pbkdf2_sha256",
    pbkdf2_sha256__default_rounds=30000
)


def login(email, password):
    users = DbConnection().select("select * from users where email = %s limit 1", (email))

    if len(users) == 0:
        raise InvalidCredentialsException()

    user = users[0]

    if not pwd_context.verify(password, user['password']):
        raise InvalidCredentialsException()

    jwt_payload = generate_jwt_payload(user)
    jwt = generate_jwt_tokens(jwt_payload)

    return {'jwt': jwt}


def refresh_jw_tokens(refresh_token):
    secret = Config().jwt['refresh_secret']
    decoded_refresh_token = JWT.decode(refresh_token, secret, algorithms='HS256')

    jwt_payload = generate_jwt_payload(decoded_refresh_token)
    jwt = generate_jwt_tokens(jwt_payload)

    return {'jwt': jwt}


def generate_jwt_payload(source):
    return {
        'access': {
            'id': source['id'],
            'exp': datetime.today() + timedelta(days=1)
        },
        'refresh': {
            'id': source['id'],
            'exp': datetime.today() + timedelta(days=30)
        }
    }


def generate_jwt_tokens(jwt_payload):
    return {
        'access': Jwt.generate_access(jwt_payload['access']),
        'refresh': Jwt.generate_refresh(jwt_payload['refresh'])
    }