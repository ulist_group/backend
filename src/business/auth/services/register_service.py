from src.infrastucture.db.db_connection import DbConnection
from src.business.auth.exceptions.user_exists_exeption import UserExistsException
from passlib.context import CryptContext

pwd_context = CryptContext(
    schemes=["pbkdf2_sha256"],
    default="pbkdf2_sha256",
    pbkdf2_sha256__default_rounds=30000
)


def register(email, password):
    users = DbConnection().select("select * from users where email = %s limit 1", (email))

    if len(users) > 0:
        raise UserExistsException()

    password_hash = pwd_context.encrypt(password)

    DbConnection().query("insert into users (email, password) values (%s, %s)", (email, password_hash))

    return {
        'email': email, 'password': password
    }
