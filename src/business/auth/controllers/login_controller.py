from src.infrastucture.rpc.controller.rpc_controller import RpcController
import src.business.auth.services.login_service as LoginService


class LoginController(RpcController):

    def login(self, email, password):
        return LoginService.login(email, password)

    def refresh_jw_tokens(self, refresh_token):
        return LoginService.refresh_jw_tokens(refresh_token)