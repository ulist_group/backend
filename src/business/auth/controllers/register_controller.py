from src.infrastucture.rpc.controller.rpc_controller import RpcController
import src.business.auth.services.register_service as RegisterService

class RegisterController(RpcController):

    def register(self, email, password):
        return RegisterService.register(email, password)