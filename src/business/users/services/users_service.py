from src.infrastucture.db.db_connection import DbConnection
from src.business.users.exceptions.user_not_found_exeption import UserNotFoundException


def get(id):
    users = DbConnection().select("select id, email from users where id = %s limit 1", (id))

    if len(users) == 0:
        raise UserNotFoundException()

    user = users[0]

    return user
