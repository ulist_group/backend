from src.infrastucture.exceptions.business_exception import BusinessException


class UserNotFoundException(BusinessException):
    def __init__(self):
        super().__init__(message='User not found', errors=[], code=1020)