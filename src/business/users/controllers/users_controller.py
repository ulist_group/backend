from src.infrastucture.rpc.controller.rpc_controller import RpcController
from src.business.users.services import users_service as UsersService


class UsersController(RpcController):

    def get(self):
        return UsersService.get(self.http_request.user['id'])