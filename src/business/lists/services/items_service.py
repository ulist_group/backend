from src.infrastucture.db.db_connection import DbConnection


def create(user_id, list_id, item_name):
    inserted = DbConnection().insert(
        "insert into list_items (list_id, name) values (%s, %s) returning id",
        (list_id, item_name)
    )
    return inserted[0]['id']


def remove(user_id, item_id):
    DbConnection().query("delete from list_items where id = %s", item_id)


def get_items(user_id, list_id):
    items = DbConnection().select(
        "select * from list_items where list_id = %s",
        list_id
    )
    return items
