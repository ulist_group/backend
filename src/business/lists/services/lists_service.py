from src.infrastucture.db.db_connection import DbConnection


def create(user_id, list_name):
    inserted = DbConnection().insert(
        "insert into lists (user_id, name) values (%s, %s) returning id",
        (user_id, list_name)
    )
    return inserted[0]['id']


def remove(user_id, list_id):
    DbConnection().query(
        "delete from lists where user_id = %s and id = %s",
        (user_id, list_id)
    )


def get_lists(user_id):
    lists = DbConnection().select(
        "select * from lists where user_id = %s",
        (user_id)
    )
    return lists


def get_list(user_id, list_id):
    lists = DbConnection().select(
        "select * from lists where user_id = %s and id = %s limit 1",
        (user_id, list_id)
    )
    return lists[0]
