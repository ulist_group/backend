from src.infrastucture.rpc.controller.rpc_controller import RpcController
from src.business.lists.services import lists_service as ListsService


class ListsController(RpcController):

    def create(self, name):
        return ListsService.create(self.http_request.user['id'], name)

    def remove(self, id):
        return ListsService.remove(self.http_request.user['id'], id)

    def get_lists(self):
        return ListsService.get_lists(self.http_request.user['id'])

    def get_list(self, id):
        return ListsService.get_list(self.http_request.user['id'], id)
