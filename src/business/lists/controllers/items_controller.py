from src.infrastucture.rpc.controller.rpc_controller import RpcController
from src.business.lists.services import items_service as ItemsService


class ItemsController(RpcController):

    def create(self, list_id, name):
        return ItemsService.create(self.http_request.user['id'], list_id, name)

    def remove(self, id):
        return ItemsService.remove(self.http_request.user['id'], id)

    def get_items(self, list_id):
        return ItemsService.get_items(self.http_request.user['id'], list_id)
