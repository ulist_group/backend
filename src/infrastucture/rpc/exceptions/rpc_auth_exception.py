class RpcAuthException(Exception):
    def __init__(self):
        self.message = 'Authorization failed'
        self.code = 100
        super().__init__(self.message)