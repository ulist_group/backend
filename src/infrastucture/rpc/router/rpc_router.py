import json, importlib, inflection
import src.const as Const

from src.infrastucture.rpc.exceptions.rpc_auth_exception import RpcAuthException
from src.infrastucture.singleton_meta import SingletonMeta

class RpcRouter(metaclass = SingletonMeta):

    def __init__(self, router_schema_path):
        with open(router_schema_path) as json_file:
            self.router_schema = json.load(json_file)

        router_schema_dir = ('/').join(router_schema_path.replace(Const.ROOT, '').split('/')[1:-1])
        business_logic_dir = router_schema_dir + self.router_schema['business_logic_dir']
        self.business_logic_path = business_logic_dir.replace('/', '.')

    def get_controller_method(self, api_method_name, http_request):
        self.check_auth(api_method_name, http_request)

        module_name, controller_path_relative, method_name = self.get_method_data(api_method_name)

        controller_file_name = controller_path_relative.split('.')[-1]
        controller_class_name = inflection.camelize(controller_file_name)
        package_path_absolute = '.'.join([
            self.business_logic_path, module_name, 'controllers', controller_path_relative
        ])
        package = importlib.import_module(package_path_absolute)
        controller_class = getattr(package, controller_class_name)
        controller = controller_class(http_request)
        return getattr(controller, method_name)


    def get_method_data(self, api_method_name):
        route = self.router_schema['routes'][api_method_name]

        module_name = route['module']
        controller_path_relative = route['controller']
        method_name = route['method']

        return module_name, controller_path_relative, method_name

    def check_auth(self, api_method_name, http_request):
        route = self.router_schema['routes'][api_method_name]

        auth_required_string = route['auth_required'] if 'auth_required' in route\
            else self.router_schema['auth_required_default']

        auth_required = auth_required_string.lower() == 'true'

        if auth_required != http_request.authenticated:
            raise RpcAuthException()

