from src.infrastucture.rpc.response.rpc_response import rpc_response


class rpc_response_result(rpc_response):
    def __init__(self, result = {}, id = None):
        rpc_response.__init__(self, id)
        self.result = result
