from src.infrastucture.rpc.response.rpc_response import rpc_response


class rpc_response_error(rpc_response):
    def __init__(self, code, message, data = None, id = None):
        rpc_response.__init__(self, id)
        self.error = {
            'code': code,
            'message': message,
            'data': data,
        }
