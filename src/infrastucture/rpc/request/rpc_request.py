import inspect
from schema import Schema, And, Optional
import src.const as const

from src.infrastucture.rpc.exceptions.rpc_auth_exception import RpcAuthException
from src.infrastucture.exceptions.business_exception import BusinessException
from src.infrastucture.rpc.router.rpc_router import RpcRouter
from src.infrastucture.rpc.response.rpc_response_error import rpc_response_error
from src.infrastucture.rpc.response.rpc_response_result import rpc_response_result

rpc_request_data_validation_schema = Schema({
    'jsonrpc': And(str, lambda jsonrpc: jsonrpc == '2.0'),
    'method': str,
    Optional('params'): dict,
    Optional('id'): int or None
})


class RpcRequest:

    def __init__(self, rpc_request_data = None):
        self.rpc_request_data = rpc_request_data
        self.validate()
        if self.is_valid:
            self.jsonrpc = rpc_request_data['jsonrpc']
            self.api_method_name = rpc_request_data['method']
            self.params = rpc_request_data['params'] if 'params' in rpc_request_data else {}
            if 'id' in rpc_request_data:
                self.id = rpc_request_data['id']

    def validate(self):
        self.validation_errors = []
        try:
            rpc_request_data_validation_schema.validate(self.rpc_request_data)
            self.is_valid = True
        except Exception as error:
            self.is_valid = False
            self.validation_errors.append(error.args)

    async def handle(self, http_request):
        if not self.is_valid:
            return rpc_response_error(-32600, 'Invalid Request', {'request': self.rpc_request_data}).__dict__

        router = RpcRouter(const.ROOT + '/src/business/router_schema.json')

        try:
            # router_method = getattr(router, inflection.underscore(self.api_method_name))
            controller_method = router.get_controller_method(self.api_method_name, http_request)
        except RpcAuthException as error:
            return rpc_response_error(error.code, error.message, {'request': self.rpc_request_data}).__dict__
        except Exception as error:
            return rpc_response_error(-32601, 'Method not found', {'request': self.rpc_request_data}).__dict__

        try:
            result = self.call_method(controller_method)
        except BusinessException as exception:
            return rpc_response_error(exception.code, exception.message, {'request': self.rpc_request_data}).__dict__
        except Exception as exception:
            return rpc_response_error(-100, exception.args[0], {'request': self.rpc_request_data}).__dict__

        return result

    def call_method(self, method):

        method_args = set(inspect.signature(method).parameters)
        diff = method_args - set(self.params)
        if len(diff) > 0:
            error_data = {'required': list(diff), 'request': self.rpc_request_data}
            return rpc_response_error(-32602, 'Invalid Params', error_data).__dict__

        result = method(**self.params) if hasattr(self, 'params') else method()
        return rpc_response_result(result).__dict__
