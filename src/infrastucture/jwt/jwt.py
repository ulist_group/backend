import jwt as JWT
from src.infrastucture.config.config import Config


def generate(payload, jwt_type):
    secret = Config().jwt[jwt_type + '_secret']
    return JWT.encode(payload, secret, algorithm='HS256').decode('utf-8')


def verify(token, jwt_type):
    secret = Config().jwt[jwt_type + '_secret']
    return JWT.decode(token, secret, algorithms='HS256')


def generate_access(payload):
    return generate(payload, 'access')


def verify_access(token):
    return verify(token,  'access')


def generate_refresh(payload):
    return generate(payload, 'refresh')


def verify_refresh(token):
    return verify(token,  'refresh')