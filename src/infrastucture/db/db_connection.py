from sqlalchemy import create_engine
from src.infrastucture.singleton_meta import SingletonMeta
from src.infrastucture.config.config import Config

class DbConnection(metaclass=SingletonMeta):

    def __init__(self):
        user = Config().db['db_user']
        password = Config().db['db_password']
        host = Config().db['db_host']
        port = Config().db['db_port']
        db_name = Config().db['db_name']
        self.engine = create_engine(f"postgresql://{user}:{password}@{host}:{port}/{db_name}")

    def query(self, query, params):
        with self.engine.connect() as con:
            return con.execute(query, params)

    def select(self, query, params):
        rows = []
        results_proxy = self.query(query, params)
        for result_proxy in results_proxy:
            rows.append(dict(result_proxy))
        return rows

    def insert(self, query, params):
        rows = []
        results_proxy = self.query(query, params)
        for result_proxy in results_proxy:
            rows.append(dict(result_proxy))
        return rows
