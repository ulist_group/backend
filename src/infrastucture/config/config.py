from src.infrastucture.singleton_meta import SingletonMeta
import configparser

class Config(metaclass=SingletonMeta):

    def __init__(self):
        config = configparser.ConfigParser()
        config.read('.env')
        self.app = config['app']
        self.db = config['db']
        self.jwt = config['jwt']
