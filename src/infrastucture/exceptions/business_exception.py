class BusinessException(Exception):
    def __init__(self, message, errors, code):
        super().__init__(message)
        self.message = message
        self.errors = errors
        self.code = code