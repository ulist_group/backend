import json, asyncio
from aiohttp import web
from schema import Schema
from src.infrastucture.config.config import Config
from src.infrastucture.rpc.request.rpc_request import RpcRequest
from src.infrastucture.rpc.response.rpc_response import rpc_response
from src.infrastucture.rpc.response.rpc_response_error import rpc_response_error
from src.infrastucture.http.middlewares.auth import auth

rpc_request_schema = Schema({
    'jsonrpc': lambda x: x == '2.0',
    'method': str,
    'params': dict,
    'id': int
})

async def handle(http_request):
    """
    Handles http request
    """

    try:
        # send error if content_type is not application/json
        if http_request.content_type != 'application/json':
            error = rpc_response_error(-32700, 'Parse error')
            return response(error)

        # get request body
        try:
            body: dict = await http_request.json()
        except Exception:
            error = rpc_response_error(-32700, 'Parse error')
            return response(error)

        # catch single/multi rpc request(s)
        if isinstance(body, list):
            rpc_requests_data = body
        else:
            rpc_requests_data = [body]

        # collect rpc requests handle procedures
        rpc_requests_tasks = []
        for rpc_request_data in rpc_requests_data:
            rpc_requests_tasks.append(
                RpcRequest(rpc_request_data).handle(http_request)
            )

        # call rpc requests handle procedures
        rpc_responses = await asyncio.gather(
            *rpc_requests_tasks
        )
    except Exception as exception:
        return response(
            rpc_response_error(-150, exception.args[0], {'request': await http_request.json()}).__dict__
        )

    return response(rpc_responses if isinstance(body, list) else rpc_responses[0])


def run():

    # Runs webserver.

    app = web.Application(middlewares=[auth])

    app.add_routes([web.post('/', handle)])

    host = Config().app['app_host']
    port = Config().app['app_port']

    web.run_app(app, host=host, port=port)


def response(something):

    # Returns http response

    jsonable = something
    if (isinstance(something, rpc_response)):
        jsonable = something.__dict__

    return web.Response(
        text = json.dumps(jsonable),
        content_type = 'application/json'
    )
