from aiohttp import web
from jwt import DecodeError, ExpiredSignatureError
from src.infrastucture.jwt import jwt


@web.middleware
async def auth(request, handler):

    request.authenticated = False
    request.user = None

    if 'authorization' not in request.headers:
        return await handler(request)

    auth_header = request.headers['authorization']

    auth_bearer, auth_token = auth_header.split(' ')

    if auth_bearer.lower() != 'bearer':
        return await handler(request)

    try:
        request.user = jwt.verify_access(auth_token)
        request.authenticated = True
    except ExpiredSignatureError:
        request.authenticated = False
        # todo: create request field containing information about expired token
    except Exception:
        request.authenticated = False

    return await handler(request)
