# Ulist (universal list)

Application allows you to create any type of lists with custom fields and link them together like relational database tables.

## Run application

### Production

To build and run application on production server, just run:
```buildoutcfg
docker-compose up
```

### Localhost

There is a special docker-compose file for localhost.
You can get it from _docker-compose-local.yml.example_.
Just delete _example_ from filename.

1. Run necessary containers run:

```buildoutcfg
docker-compose -f docker-compose-local.yml up
```

3. Prepare (dependencies and migrations):

```buildoutcfg
sh start-local.sh
```

4. Run application:

```buildoutcfg
python3 __main__.py
```


