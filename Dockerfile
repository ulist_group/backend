FROM python:latest

WORKDIR /app

COPY ./ /app

EXPOSE 9191

ENTRYPOINT ["sh", "start.sh"]
