"""add users table"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5a96c70e65c6'
down_revision = None
branch_labels = None
depends_on = None

conn = op.get_bind()


def upgrade():
    conn.execute('''
        create table if not exists users (
            id serial primary key,
            email varchar(40) NOT NULL,
            password varchar(128) NOT NULL
        );
    ''')


def downgrade():
    con.execute('drop table if exists users;')
    con.execute('drop sequence users_id_seq;')
