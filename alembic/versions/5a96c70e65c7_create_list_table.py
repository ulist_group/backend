"""create lists table"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5a96c70e65c7'
down_revision = None
branch_labels = None
depends_on = None

conn = op.get_bind()


def upgrade():
    conn.execute('''
            create table if not exists lists (
                id serial primary key,
                user_id int NOT NULL,
                name varchar(40) NOT NULL
            );
    ''')


def downgrade():
    con.execute('drop table if exists lists;')
    con.execute('drop sequence lists_id_seq;')
