"""create list items table"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5a96c70e65c8'
down_revision = None
branch_labels = None
depends_on = None

conn = op.get_bind()


def upgrade():
    conn.execute('''
        create table if not exists list_items (
            id serial primary key,
            list_id int NOT NULL,
            name varchar(60) NOT NULL
        );
    ''')


def downgrade():
    con.execute('drop table if exists list_items;')
    con.execute('drop sequence list_items_id_seq;')
